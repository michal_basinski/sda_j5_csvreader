package sda.files.reader;

import java.util.List;
import java.util.Map;

public interface ISDAFileReader {
    List<Map<String, String>> readFile(String filePath) throws SDAFileReaderException;
}
