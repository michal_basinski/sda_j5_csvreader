package sda.files.reader;

public class SDAFileReaderException extends Throwable {

    public SDAFileReaderException(String message, Throwable cause) {
        super(message, cause);
    }

}
