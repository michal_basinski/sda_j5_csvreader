package sda.files.reader;

import java.util.List;
import java.util.Map;

public class App
{
    public static void main( String[] args ) {
        String filePath = "/home/michal/plik.csv";

        ISDAFileReader ISDAFileReader = new CSVFileReader();

        try {
            List<Map<String, String>> fileContents = ISDAFileReader.readFile(filePath);
            System.out.println("Liczba wierszy w pliku: " + fileContents.size());
        } catch (SDAFileReaderException e) {
            e.printStackTrace();
        }


    }
}
